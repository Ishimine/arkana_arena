﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealth
{
    int Health
    {
        get;
    }

    int MaxHealth
    {
        get;
    }

    //Returns true if damage is letal
    bool GetDamage(int ammount);

    bool Heal(int ammount);

}
