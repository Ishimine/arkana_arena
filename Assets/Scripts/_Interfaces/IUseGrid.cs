﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IUseGrid
{
    void JumpToGridPosition(int x, int y);
    void DisplaceToDirection(Vector2Int displacement, Action postAction = null);
    void KnockBack(Vector2 source, int ammount);
}
