﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnergySystem
{

    public abstract float Energy { get; }
    public abstract void SetMaxEnergy(float value);
    public abstract float GetMaxEnergy();
    public abstract bool ConsumeEnergy(float ammount);
    public abstract void Update();


}
