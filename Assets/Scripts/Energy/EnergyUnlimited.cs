﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyUnlimited : EnergySystem
{
    private float maxEnergy = float.MaxValue;

    public override float Energy
    {
        get
        {
            return GetMaxEnergy();
        }
    }

    public override bool ConsumeEnergy(float ammount)
    {
        return true;
    }

    public override float GetMaxEnergy()
    {
        return maxEnergy;
    }

    public override void SetMaxEnergy(float value)
    {
    }

    public override void Update()
    {
    }
}
