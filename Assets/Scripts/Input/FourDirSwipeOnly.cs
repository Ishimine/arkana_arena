﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;
using Sirenix.OdinInspector;

namespace ControllerSystem
{

    [CreateAssetMenu(fileName = "InterpreterV1", menuName = "PlayerController/InterpreterV1")]
    public class FourDirSwipeOnly : Interpreter
    {
        public bool swipeOnDelta = false;
        public float deltaLength = 10;

        [SerializeField,ReadOnly]
        private bool deltaDone = false;

        public LeanFingerSwipeAccion goLeft;
        public LeanFingerSwipeAccion goRight;
        public LeanFingerSwipeAccion goForward;
        public LeanFingerSwipeAccion goBack;

        public LeanFingerTapAccion fireLeft;
        public LeanFingerTapAccion fireRight;

        public override void Initialize(PlayerController myController)
        {
            base.Initialize(myController);

            goLeft.Initialize(null, null, myController.GoLeft);
            goRight.Initialize(null, null, myController.GoRight);
            goForward.Initialize(null, null, myController.GoForward);
            goBack.Initialize(null, null, myController.GoBack);

            goLeft.CheckAngle = true;
            goLeft.Angle = 270;
            goLeft.AngleThreshold = 90;
            goRight.CheckAngle = true;
            goRight.Angle = 90;
            goRight.AngleThreshold = 90;
            goForward.CheckAngle = true;
            goForward.Angle = 0;
            goForward.AngleThreshold = 90;
            goBack.CheckAngle = true;
            goBack.Angle = 180;
            goBack.AngleThreshold = 90;

            goLeft.checkDeltaLength = swipeOnDelta;
            goLeft.deltaLenght = deltaLength;
            goRight.checkDeltaLength = swipeOnDelta;
            goRight.deltaLenght = deltaLength;
            goForward.checkDeltaLength = swipeOnDelta;
            goForward.deltaLenght = deltaLength;
            goBack.checkDeltaLength = swipeOnDelta;
            goBack.deltaLenght = deltaLength;

            fireLeft.Initialize(null,myController.FireLeft);
            fireRight.Initialize(null, myController.FireRight);

            SetActive(true);
        }

        public override void TranslateDown(LeanFinger down)
        {
        }

        public override void TranslateSet(LeanFinger set)
        {
            if (!isActive || !swipeOnDelta || deltaDone) return;

            //            Debug.Log("Set");
            deltaDone |= goLeft.FingerSwipe(set);
            deltaDone |= goRight.FingerSwipe(set);
            deltaDone |= goForward.FingerSwipe(set);
            deltaDone |= goBack.FingerSwipe(set);
        }

        public override void TranslateSwipe(LeanFinger swipe)
        {
            if (!isActive || swipeOnDelta) return;

            goLeft.FingerSwipe(swipe);
            goRight.FingerSwipe(swipe);
            goForward.FingerSwipe(swipe);
            goBack.FingerSwipe(swipe);
        }

        public override void TranslateTap(LeanFinger tap)
        {
            //Debug.Log("TranslateTap");
            if (!isActive) return;

            fireLeft.FingerTap(tap);
            fireRight.FingerTap(tap);
        }

        public override void TranslateUp(LeanFinger up)
        {
            //Debug.Log("TranslateUp");
            deltaDone = false;
        }
    }


}
