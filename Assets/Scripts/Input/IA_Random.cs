﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;
using System.Timers;
using Sirenix.OdinInspector;

namespace ControllerSystem
{

    [CreateAssetMenu(fileName = "RandomIA", menuName = "PlayerController/RandomIA")]
    public class IA_Random : Interpreter
    {
        public Vector2 minMaxInterval = new Vector2(100, 1000);

        bool active = false;
        [SerializeField, ReadOnly]
        float time;
        float lastInterval;

        [Button]
        public void Start()
        {
            time = GetRandom();
            active = true;
        }

        [Button]
        public void Stop()
        {
            active = false;
        }

        public override void Initialize(PlayerController myController)
        {
            base.Initialize(myController);
            UniversalUpdate.instantiate.GlobalUpdate -= Update;
            UniversalUpdate.instantiate.GlobalUpdate += Update;
            Start();
        }

        private float GetRandom()
        {
            return UnityEngine.Random.Range(minMaxInterval.x, minMaxInterval.y);
        }

        private void RandomAction()
        {
            if (!active) return;
            int opc = UnityEngine.Random.Range(0, 6);
            switch (opc)
            {
                case 0:
                    myController.GoLeft();
                    break;
                case 1:
                    myController.GoRight();
                    break;
                case 2:
                    myController.GoForward();
                    break;
                case 3:
                    myController.GoBack();
                    break;
                case 4:
                    myController.FireLeft();
                    break;
                case 5:
                    myController.FireRight();
                    break;
            }
        }


        void Update()
        {
            if (!active) return;
            if (time <= 0)
            {
                RandomAction();
                time = GetRandom();
            }
            else
            {
                time -= Time.deltaTime;
            }
        }

        public override void TranslateSwipe(LeanFinger swipe)
        {
        }

        public override void TranslateTap(LeanFinger tag)
        {
        }

        public override void TranslateSet(LeanFinger set)
        {
        }

        public override void TranslateUp(LeanFinger up)
        {
        }

        public override void TranslateDown(LeanFinger down)
        {
        }
    }
}
