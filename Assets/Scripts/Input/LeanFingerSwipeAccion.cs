﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using System;
using Sirenix.OdinInspector;

[Serializable]
public class LeanFingerSwipeAccion
{
    public enum ClampType
    {
        None,
        Normalize,
        Direction4,
        ScaledDelta
    }

    public bool checkDeltaLength = false;
    public float deltaLenght = 10;

    [Tooltip("Ignore fingers with StartedOverGui?")]
    public bool IgnoreStartedOverGui = true;

    [Tooltip("Ignore fingers with IsOverGui?")]
    public bool IgnoreIsOverGui;

    [Tooltip("Do nothing if this LeanSelectable isn't selected?")]
    public LeanSelectable RequiredSelectable;

    [Tooltip("Must the swipe be in a specific direction?")]
    public bool CheckAngle;

    [Tooltip("The required angle of the swipe in degrees, where 0 is up, and 90 is right")]
    public float Angle;

    [Tooltip("The left/right tolerance of the swipe angle in degrees")]
    public float AngleThreshold = 90.0f;

    [Tooltip("Should the swipe delta be modified before use?")]
    public ClampType Clamp;

    [Tooltip("The swipe delta multiplier, useful if you're using a Clamp mode")]
    public float Multiplier = 1.0f;

    // Called on the first frame the conditions are met

    public delegate void TriggerEvent();

    private FingerEvent OnSwipe;
    private Vector2Event OnSwipeDelta;
    private TriggerEvent OnSwipeTrigger;


    //TriggerEvent OnSwipeTrigger;
    //FingerEvent onSwipeResponse;
    //Vector2Event onSwipeDelta;

    public void Initialize(FingerEvent nOnSwipeResponse, Vector2Event nOnSwipeDelta, TriggerEvent nOnSwipeTrigger)
    {
        OnSwipe = nOnSwipeResponse;
        OnSwipeDelta = nOnSwipeDelta;
        OnSwipeTrigger = nOnSwipeTrigger;
        //OnEnable();
    }

    protected bool CheckSwipe(LeanFinger finger, Vector2 swipeDelta)
    {
        // Invalid angle?
        if (CheckAngle == true)
        {
            var angle = Mathf.Atan2(swipeDelta.x, swipeDelta.y) * Mathf.Rad2Deg;
            var delta = Mathf.DeltaAngle(angle, Angle);

            if (delta < AngleThreshold * -0.5f || delta >= AngleThreshold * 0.5f)
            {
                return false;
            }
        }

        // Clamp delta?
        switch (Clamp)
        {
            case ClampType.Normalize:
                {
                    swipeDelta = swipeDelta.normalized;
                }
                break;

            case ClampType.Direction4:
                {
                    if (swipeDelta.x < -Mathf.Abs(swipeDelta.y)) swipeDelta = -Vector2.right;
                    if (swipeDelta.x > Mathf.Abs(swipeDelta.y)) swipeDelta = Vector2.right;
                    if (swipeDelta.y < -Mathf.Abs(swipeDelta.x)) swipeDelta = -Vector2.up;
                    if (swipeDelta.y > Mathf.Abs(swipeDelta.x)) swipeDelta = Vector2.up;
                }
                break;

            case ClampType.ScaledDelta:
                {
                    swipeDelta *= LeanTouch.ScalingFactor;
                }
                break;
        }


        if(checkDeltaLength)
        {
            //Debug.Log("Magnitude: " + swipeDelta.magnitude);
            if (deltaLenght > swipeDelta.magnitude)
            {
                return false;
            }
        }


        // Call event
        if (OnSwipe != null)
        {
            OnSwipe.Invoke(finger);
        }

        if(OnSwipeTrigger != null)
        {
            OnSwipeTrigger.Invoke();
        }

        if (OnSwipeDelta != null)
        {
            OnSwipeDelta.Invoke(swipeDelta * Multiplier);
        }

        return true;
    }

    public bool FingerSwipe(LeanFinger finger)
    {
        // Ignore?
        if (IgnoreStartedOverGui == true && finger.StartedOverGui == true)
        {
            return false;
        }

        if (IgnoreIsOverGui == true && finger.IsOverGui == true)
        {
            return false;
        }

        if (RequiredSelectable != null && RequiredSelectable.IsSelected == false)
        {
            return false;
        }

        // Perform final swipe check and fire event
        return CheckSwipe(finger, finger.SwipeScreenDelta);
    }
}
