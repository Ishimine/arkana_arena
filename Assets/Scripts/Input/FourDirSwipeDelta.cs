﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;
namespace ControllerSystem
{
    public class FourDirSwipeDelta : Interpreter
    {
        public override void TranslateDown(LeanFinger down)
        {
        }

        public override void TranslateSet(LeanFinger set)
        {
        }

        public override void TranslateSwipe(LeanFinger swipe)
        {
        }

        public override void TranslateTap(LeanFinger tag)
        {
        }

        public override void TranslateUp(LeanFinger up)
        {
        }
    }
}
