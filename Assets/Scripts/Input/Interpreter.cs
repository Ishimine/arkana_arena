﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using Sirenix.OdinInspector;

namespace ControllerSystem
{


public abstract class Interpreter : SerializedScriptableObject
{
        protected bool isActive = false;
        public bool IsActive
        {
            get
            {
                return isActive;
            }
        }

        public virtual void SetActive(bool value)
        {
            isActive = value;
        }

    protected PlayerController myController;
    public virtual void Initialize(PlayerController myController)
    {
        this.myController = myController;
    }
    public abstract void TranslateSwipe(LeanFinger swipe);
    public abstract void TranslateTap(LeanFinger tag); 
    public abstract void TranslateSet(LeanFinger set); 
    public abstract void TranslateUp(LeanFinger up); 
    public abstract void TranslateDown(LeanFinger down);
    }
}



// Event signature
public delegate void FingerEvent(LeanFinger value);
public delegate void Vector2Event(Vector2 value);
