﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Lean.Touch;

namespace ControllerSystem
{

    [CreateAssetMenu(fileName = "Controller", menuName = "PlayerController/Controller")]
    public class PlayerController : SerializedScriptableObject
    {
        private bool isActive = false;

        [HideInInspector]
        public delegate void BoolEvent(bool value);
        [HideInInspector]
        public BoolEvent playerOnActiveChange;

        [SerializeField]
        public Unit unit;

        public Interpreter interpreter;

        #region Configuration

        public void PlayerInitialize(Unit unit)
        {
            Debug.Log("PlayerController " + this + ": Initialized");
            SetUnit(unit);
            interpreter.Initialize(this);
            SetControllerActive();
        }

        public void SetUnit(Unit nUnit)
        {
            unit = nUnit;
        }

        private void SetControllerActive(bool value)
        {
            if (isActive != value)
            {
                isActive = value;
                if (playerOnActiveChange != null)
                    playerOnActiveChange.Invoke(value);
            }
            Debug.Log(this.name + " is " + isActive);
        }

        [Button]
        public void SetControllerActive()
        {
            LeanTouch.OnFingerSwipe += FingerSwipe;
            LeanTouch.OnFingerTap += FingerTap;
            LeanTouch.OnFingerSet += OnFingerSet;
            LeanTouch.OnFingerDown += OnFingerDown;
            LeanTouch.OnFingerUp += OnFingerUp;
            SetControllerActive(true);
        }

        [Button]
        public void SetControllerInactive()
        {
            LeanTouch.OnFingerSwipe -= FingerSwipe;
            LeanTouch.OnFingerTap -= FingerTap;
            LeanTouch.OnFingerSet -= OnFingerSet;
            LeanTouch.OnFingerDown -= OnFingerDown;
            LeanTouch.OnFingerUp -= OnFingerUp;
            SetControllerActive(false);
        }

        public void FingerSwipe(LeanFinger swipe)
        {
            interpreter.TranslateSwipe(swipe);
        }

        public void FingerTap(LeanFinger tap)
        {
            interpreter.TranslateTap(tap);
        }

        public void OnFingerSet(LeanFinger tap)
        {
            interpreter.TranslateSet(tap);
        }

        public void OnFingerDown(LeanFinger tap)
        {
            interpreter.TranslateDown(tap);
        }

        public void OnFingerUp(LeanFinger tap)
        {
            interpreter.TranslateUp(tap);
        }

        #endregion

        #region Gameplay

        public void Move(Vector2Int dir)
        {
            unit.DisplaceToDirection(dir);
        }

        [Button]
        public void GoRight()
        {
            if (!isActive) return;
            unit.GoRight();
        }

        [Button]
        public void GoLeft()
        {
            if (!isActive) return;
            unit.GoLeft();
        }

        [Button]
        public void GoForward()
        {
            if (!isActive) return;
            unit.GoForward();
        }

        [Button]
        public void GoBack()
        {
            if (!isActive) return;
            unit.GoBack();
        }

        public void FireRight()
        {
            if (!isActive) return;
            unit.RightFire();
        }

        public void FireLeft()
        {
            if (!isActive) return;
            unit.LeftFire();
        }

        #endregion
    }
}
