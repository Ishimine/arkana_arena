﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using ControllerSystem;
using System;

public class Unit : SerializedMonoBehaviour, IHealth, IUseGrid
{
    [TabGroup("Gameplay")]
    public bool lookingUp;

    [TabGroup("Gameplay")]
    public float fireCost = 10;

    [TabGroup("Gameplay")]
    public float recoveryTime = .7f;

    [TabGroup("Gameplay")]
    public EnergySystem energy = new EnergyUnlimited();

    [TabGroup("Gameplay")]
    public Shooter leftShooter;

    [TabGroup("Gameplay")]
    public Shooter rightShooter;

    [TabGroup("Dependecys")]
    public PlayerController controller;
    public bool onEnableStartInCenter;
    [TabGroup("Dependecys")]
    public BattleGrid grid;
    [TabGroup("Dependecys")]
    public GameObject proyectile;

    [TabGroup("Dependecys")]
    public Rigidbody2D  rb;

    [TabGroup("Dependecys")]
    public Unit_Visual visual;

    [SerializeField, ReadOnly]
    private Vector2Int myGridPosition;

    private CodeAnimator freezeTimer = new CodeAnimator();
    private CodeAnimator positionAnimator = new CodeAnimator();

    [TabGroup("Animation")]
    public AnimationCurve positionTransitionCurve;
    [TabGroup("Animation")]
    public float positionTransitionDuration = .5f;

    

    [SerializeField]
    private int health = 5;
    public int Health
    {
        get
        {
            return health;
        }
    }

    [SerializeField]
    private int maxHealth = 5;
    public int MaxHealth
    {
        get
        {
            return maxHealth;
        }
    }

    [Button]
    public void Initialize()
    {
        Debug.Log(name + " player initialized");
        controller.PlayerInitialize(this);

        if (onEnableStartInCenter && grid != null)
            JumpToGridPosition(grid.GetCenterGrid());

        rightShooter.Initialize(this,proyectile);
      //  rightShooter.localPosition = new Vector3(1.5f, 2);

        leftShooter.Initialize(this, proyectile);
      //  leftShooter.localPosition = new Vector3(-1.5f, 2);
    }

    public void Initialize(BattleGrid grid)
    {
        Debug.Log(name + " player initialized");
        this.grid = grid;
        controller.PlayerInitialize(this);
        if (onEnableStartInCenter && grid != null)
            JumpToGridPosition(grid.GetCenterGrid());
    }

    public void JumpToGridPosition(Vector2Int nGridPos, Action postAction = null)
    {
        nGridPos = ClampGridPosition(nGridPos);
        myGridPosition = nGridPos;
        TransitionTo(grid.GetPosition(nGridPos.x, nGridPos.y));
    }

    public void JumpToGridPosition(int x, int y)
    {
        JumpToGridPosition(new Vector2Int(x,y));
    }

    public void TransitionTo(Vector3 nPos, int gridDistance = 1, Action postAction = null)
    {
        Vector3 pInitial = transform.position;
        Vector3 dif = nPos - pInitial;

        positionAnimator.StartAnimacion(this, x =>
        {
            transform.position = pInitial + dif * x;
        }, DeltaTimeType.deltaTime, AnimationType.Simple, positionTransitionCurve, postAction, positionTransitionDuration * gridDistance);
    }


    public void DisplaceToDirection(Vector2Int displacement, Action postAction = null)
    {
        JumpToGridPosition(myGridPosition + displacement, postAction);
    }

    [Button]
    public void GoRight()
    {
       // Debug.Log("GoRight");
        DisplaceToDirection(Vector2Int.right);
    }

    [Button]
    public void GoLeft()
    {
       // Debug.Log("GoLeft");
        DisplaceToDirection(Vector2Int.left);
    }


    [Button]
    public void GoForward()
    {
       // Debug.Log("GoForward");
        DisplaceToDirection(Vector2Int.up);
    }

    [Button]
    public void GoBack()
    {
       // Debug.Log("GoBack");
        DisplaceToDirection(Vector2Int.down);
    }

    private void OnEnable()
    {
        Initialize();
    }

    public Vector2Int ClampGridPosition(Vector2Int gridPosition)
    {
        return new Vector2Int(Mathf.Clamp(gridPosition.x, 0, grid.gridCells.x - 1),Mathf.Clamp(gridPosition.y, 0, grid.gridCells.y - 1));
    }

    [Button]
    public void LeftFire()
    {
        if(energy.ConsumeEnergy(fireCost))
        {
            leftShooter.Fire();
        }
    }

    [Button]
    public void RightFire()
    {
        if (energy.ConsumeEnergy(fireCost))
        {
            rightShooter.Fire();
        }
    }


    public bool GetDamage(int ammount)
    {
        health -= ammount;
        bool isLetal = health <= 0;

        SetFreezeState(true, recoveryTime);
        visual.Damaged(recoveryTime);

        if (isLetal)
        {
            health = 0;
            Dead();
        }
        return isLetal;
    }

    public void SetFreezeState(bool value, float duration = 0)
    {
        if(value)
            controller.SetControllerInactive();
        else
            controller.SetControllerActive();

        if (duration > 0 && health > 0)
        {
            if (value)
                freezeTimer.StartWaitAndExecute(this,duration,()=> controller.SetControllerActive(), false);
            else
                freezeTimer.StartWaitAndExecute(this, duration, () => controller.SetControllerInactive(), false);

        }
    }

    public void FreezeController()
    {
        SetFreezeState(true);
    }

    public void UnfreezeController()
    {
        SetFreezeState(false);
    }

    public bool Heal(int ammount)
    {
        health += ammount;
        if (health > maxHealth)
            health = ammount;

        return false;
    }

    public void KnockBack(Vector2 source, int ammount)
    {
        Vector2 dir = (Vector2)transform.position - source;
        if(dir.x > dir.y)
            dir.y = 0;
        else
            dir.x = 0;

        dir.Normalize();
        DisplaceToDirection(new Vector2Int(Mathf.RoundToInt(dir.x), Mathf.RoundToInt(dir.y)) * ammount, null);
    }

    private void Dead()
    {
        visual.Disappear();
        rb.simulated = false;
        FreezeController();
        positionAnimator.StartWaitAndExecute(this, 3, Revive, false);
    }

    private void Revive()
    {
        transform.position = grid.GetPosition(grid.GetCenterGrid());
        rb.simulated = true;
        visual.Appear();
        health = maxHealth;
        SetFreezeState(true,1);
    }
}
