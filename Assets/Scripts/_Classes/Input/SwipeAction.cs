﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class SwipeAction : TouchAction
{
    private Vector2 pInitial;
    public Vector2 InitialPosition
    {
        get { return pInitial; }
        set { pInitial = value; }
    }

    private Vector2 pFinal;
    public Vector2 FinalPosition
    {
        get { return pFinal; }
        set { pFinal = value; }
    }

    [SerializeField,ReadOnly]
    private Vector2 direction;
    public Vector2 Direction
    {
        get {   return direction;     }
        set {   direction = value;    }
    }

    public float Lenght
    {
        get { return Vector2.Distance(pInitial,pFinal); }
    }


}
