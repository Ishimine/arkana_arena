﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class TouchAction
{
    private Vector2 position;
    public Vector2 Position
    {
        get { return position; }
        set { position = value; }
    }

}
