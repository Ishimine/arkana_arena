﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


[CreateAssetMenu(fileName = "GameManager", menuName = "GameManager")]
public class GameManager : SerializedScriptableObject
{

    private GameState gameState;
    public GameState State
    {
        get
        {
            return gameState;
        }
    }




    public void GoToMenu()
    {

    }


    public void GoToMatch()
    {

    }

    public void GoToSettings()
    {

    }


    public void QuitMatch()
    {

    }

    public void LoadGame()
    {

    }

    public void SaveGame()
    {

    }

    public void StartNewGame()
    {

    }
}
