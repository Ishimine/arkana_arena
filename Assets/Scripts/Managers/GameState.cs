﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState
{

    public bool inPause = false;
    public bool inPreGame = false;
    public bool inGame = false;
    public bool inPostGame = false;
    public bool inMenu = false;
    public bool inSettings = false;

}
