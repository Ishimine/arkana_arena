﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public abstract class Proyectile : MonoBehaviour
{

    public float lifeTime = 5;

    [SerializeField]
    private float timeLeft;

    protected bool active = false;
    public bool Active
    {
        get
        {
            return active;
        }
    }
    public int damage = 1;
    public float speed = 5;

    [Button]
    public virtual void Activate()
    {
        timeLeft = lifeTime;
    }

    public virtual void Update()
    {
        if (!active) return;

        if (timeLeft <= 0)
            Dead();
        else
            timeLeft -= Time.deltaTime;
    }

    public abstract void Dead();
}
