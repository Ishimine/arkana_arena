﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalUpdate : MonoBehaviour
{
    public static UniversalUpdate instantiate;

    public delegate void VoidEvent();

    public VoidEvent GlobalUpdate;
    public VoidEvent GlobalLateUpdate;
    public VoidEvent GlobalFixedUpdate;


    private void Awake()
    {
        if (instantiate == null)
            instantiate = this;
        else
            Destroy(this.gameObject);
    }

    void Update () {
        if (GlobalUpdate != null)
            GlobalUpdate.Invoke();
    }

    void LateUpdate()
    {
        if (GlobalLateUpdate != null)
            GlobalLateUpdate.Invoke();
    }

    void FixedUpdate()
    {
        if (GlobalFixedUpdate != null)
            GlobalFixedUpdate.Invoke();
    }


}
