﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public float cooldown;
    private float timeLeft;

    public Vector3 localPosition;

    public GameObject prefab;
    Unit owner;

    private Proyectile last;

    public bool InCD
    {
        get
        {
            return timeLeft > 0;
        }
    }

    public void Initialize(Unit owner, GameObject proyectile)
    {
        this.owner = owner;
        prefab = proyectile;
    }

    public bool Fire()
    {
        if (InCD) return false;
        timeLeft = cooldown;
        last = MonoBehaviour.Instantiate(prefab, null).GetComponent<Proyectile>();
        last.transform.position = transform.position + localPosition;
        last.Activate();
        last.transform.rotation = this.transform.rotation;
        return true;
    }


    private void Update()
    {
        if(timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
    }

}
