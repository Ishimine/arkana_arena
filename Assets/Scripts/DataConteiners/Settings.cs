﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Audio;


[CreateAssetMenu(fileName = "Settings", menuName = "Settings")]
public class Settings : SerializedScriptableObject
{

    #region Audio

    [SerializeField, TabGroup("Audio")]
    private AudioMixer masterMiner;

    [SerializeField, TabGroup("Audio")]
    private bool music;
    public bool Music
    {
        get { return music; }
        set { music = value; }
    }

    [SerializeField, TabGroup("Audio")]
    private bool soundFx;
    public bool SoundFx
    {
        get { return soundFx; }
        set { soundFx = value; }
    }

    [SerializeField, TabGroup("Audio")]
    private bool ambience;
    public bool Ambience
    {
        get { return ambience; }
        set { ambience = value; }
    }

    [OnValueChanged("OnMusicVolumeChange")]
    private float musicVolume;
    public float MusicVolume
    {
        get { return musicVolume; }
        set { musicVolume = value; OnMusicVolumeChange(); }
    }

    [OnValueChanged("OnSoundFxVolumeChange")]
    private float soundFxVolume;
    public float SoundFxVolume
    {
        get { return soundFxVolume; }
        set { soundFxVolume = value; OnSoundFxVolumeChange(); }
    }

    [OnValueChanged("OnAmbienceVolumeChange")]
    private float ambienceVolume;
    public float AmbienceVolume
    {
        get { return ambienceVolume; }
        set { ambienceVolume = value; OnAmbienceVolumeChange(); }
    }

    public void OnMusicVolumeChange()
    {

    }

    public void OnSoundFxVolumeChange()
    {

    }

    public void OnAmbienceVolumeChange()
    {

    }



    #endregion

    #region Graphics
    [SerializeField, TabGroup("Graphics")]
    private bool particles;
    public bool Particles
    {
        get { return particles; }
        set { particles = value; }
    }

    [SerializeField, TabGroup("Graphics")]
    private bool trails;
    public bool Trails
    {
        get { return trails; }
        set { trails = value; }
    }

    [SerializeField, TabGroup("Graphics")]
    private bool backgroundAnimated;
    public bool BackgroundAnimated
    {
        get { return backgroundAnimated; }
        set { backgroundAnimated = value; }
    }
    #endregion


    #region Gameplay
    [SerializeField, TabGroup("Gameplay")]
    private bool swipeInputOn;
    public bool SwipeInputOn
    {
        get
        {
            return swipeInputOn;
        }
        set
        {
            swipeInputOn = value;
        }
    }


    #endregion

    #region Debug



    #endregion

}
