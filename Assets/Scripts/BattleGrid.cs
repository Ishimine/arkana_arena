﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BattleGrid : SerializedMonoBehaviour
{
    public GameObject cellPrefab;

    [HideInInspector]
    public GameObject[,] cellVisuals;

    [OnValueChanged("GridCheck")]
    public Vector2Int gridCells = new Vector2Int(3, 2);

    public bool useManualGridSize = false;
    [ShowIf("useManualGridSize")]
    public float cellSize = 2;

    public void GridCheck()
    {
        if (gridCells.x <= 0) gridCells.x = 1;
        if (gridCells.y <= 0) gridCells.y = 1;
    }

    public Vector3 GetPosition(int x, int y)
    {
        return GetPosition(new Vector2Int(x, y));
    }

    public Vector3 GetPosition(Vector2Int posId)
    {
        posId.x = Mathf.Clamp(posId.x, 0, gridCells.x-1);
        posId.y = Mathf.Clamp(posId.y, 0, gridCells.y-1);

        //Iniciamos en la esquina superior izquierda de la celda
        Vector2 displacement = -(Vector2)gridCells * cellSize / 2 ;
        //Posicionamos en el medio de la primera celda
        displacement += cellSize * Vector2.one / 2;
        //posicionamos en la celda correcta
        displacement += (Vector2)posId * cellSize;
        return transform.position + (Vector3)displacement;
    }


    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        for (int x = 0; x < gridCells.x; x++)
        {
            for (int y = 0; y < gridCells.y; y++)
            {
                Gizmos.DrawWireCube(GetPosition(x, y),cellSize * Vector3.one);
            }
        }
    }


    [Button]
    private void Instantiate()
    {
        /* if (cellVisuals != null)
         {
             foreach (GameObject go in cellVisuals)
             {
                 
             }
         }*/

        for (int i = transform.childCount-1; i >= 0; i--)
        {
            if (Application.isPlaying) Destroy(transform.GetChild(i).gameObject);
            else DestroyImmediate(transform.GetChild(i).gameObject);
        }

        cellVisuals = new GameObject[gridCells.x,gridCells.y];

        for (int x = 0; x < gridCells.x; x++)
        {
            for (int y = 0; y < gridCells.y; y++)
            {
                cellVisuals[x, y] = Instantiate(cellPrefab, this.transform);
                cellVisuals[x, y].transform.localScale = Vector3.one * cellSize;
                cellVisuals[x, y].transform.position = GetPosition(x, y);
                cellVisuals[x,y].name = "Cell (" + x + "," + y + ")";
            }
        }
    }


    [Button]
    public void ShowCells()
    {
        SetVisualsActive(true);
    }

    [Button]
    public void HideCells()
    {
        SetVisualsActive(false);
    }

    public void SetVisualsActive(bool value)
    {
        foreach (GameObject cell in cellVisuals)
        {
            cell.SetActive(value);
        }
    }

    public Vector2Int GetCenterGrid()
    {
        return new Vector2Int(Mathf.RoundToInt( gridCells.x / 2), Mathf.RoundToInt(gridCells.y / 2));
    }

}
