using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Lean.Touch
{
	// This script calls the OnFingerTap event when a finger taps the screen
	public class LeanFingerTapAccion
	{
		// Event signature

        public delegate void LeanFingerEvent(LeanFinger finger);

        public bool checkScreenSide = false;

        public bool checkLeftRight = true;
        public bool respondToLeftSide = false;
        public bool checkUpDown = false;
        public bool respondToUpSide = true;


        [Tooltip("Ignore fingers with StartedOverGui?")]
		public bool IgnoreStartedOverGui = true;

		[Tooltip("Ignore fingers with OverGui?")]
		public bool IgnoreIsOverGui;

		[Tooltip("How many times must this finger tap before OnFingerTap gets called? (0 = every time)")]
		public int RequiredTapCount = 0;

		[Tooltip("How many times repeating must this finger tap before OnFingerTap gets called? (e.g. 2 = 2, 4, 6, 8, etc) (0 = every time)")]
		public int RequiredTapInterval;

		[Tooltip("Do nothing if this LeanSelectable isn't selected?")]
		public LeanSelectable RequiredSelectable;

        public delegate void VoidEvent();
		public LeanFingerEvent OnTap;
        public VoidEvent OnTapEvent;


        public void Initialize(LeanFingerEvent onTap, VoidEvent onTapEvent)
        {
            OnTap = onTap;
            OnTapEvent = onTapEvent;
        }

		public bool FingerTap(LeanFinger finger)
		{
			// Ignore?
			if (IgnoreStartedOverGui == true && finger.StartedOverGui == true)
			{
				return false;
			}

			if (IgnoreIsOverGui == true && finger.IsOverGui == true)
			{
				return false;
			}

			if (RequiredTapCount > 0 && finger.TapCount != RequiredTapCount)
			{
                return false;
			}

			if (RequiredTapInterval > 0 && (finger.TapCount % RequiredTapInterval) != 0)
			{
                return false;
			}

			if (RequiredSelectable != null && RequiredSelectable.IsSelected == false)
			{
                return false;
			}


            if(checkScreenSide)
            {
                if (checkLeftRight)
                {
                    if (respondToLeftSide ^ finger.InLeftSide)
                    {
                        return false;
                    }
                }
                if(checkUpDown)
                {
                    if (respondToUpSide ^ finger.InUpSide)
                    {
                        return false;
                    }
                }
            }

			// Call event
			if (OnTap != null)
			{
				OnTap.Invoke(finger);
			}

            if (OnTapEvent != null)
            {
                OnTapEvent.Invoke();
            }

            return true;
		}
	}
}