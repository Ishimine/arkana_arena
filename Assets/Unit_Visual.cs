﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Unit_Visual : SerializedMonoBehaviour
{
    public SpriteRenderer renderShine;
    public SpriteRenderer render;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public float damageShineDuration = 1f;
    public AnimationCurve damageShineCurve;

    public float disappearDuration = .45f;
    public AnimationCurve disappearCurve;

    public float appearDuration = .45f;
    public AnimationCurve appearCurve;
    public IColor colorShine;

    [Button]
    public void Damaged()
    {
        Damaged(damageShineDuration);
    }

    public void Damaged(float duration)
    {
        Color cInitial = Color.clear;
        Color cFinal = colorShine.Value;
        codeAnimator.StartAnimacion(this, x =>
        {
            renderShine.color = Color.Lerp(cInitial, cFinal, x);
        }, DeltaTimeType.deltaTime, AnimationType.Simple, damageShineCurve, ()=> { renderShine.color = cInitial; }, duration);
    }


    [Button]
    public void Disappear()
    {
        Vector3 sInitial = transform.localScale;
        
        codeAnimator.StartAnimacion(this, x =>
        {
            transform.localScale = sInitial + -sInitial * x;
        }, DeltaTimeType.deltaTime, AnimationType.Simple, disappearCurve, () => { transform.localScale = Vector3.zero; }, disappearDuration);
    }

    [Button]
    public void Appear()
    {

        codeAnimator.StartAnimacion(this, x =>
        {
            transform.localScale = Vector3.one * x;
        }, DeltaTimeType.deltaTime, AnimationType.Simple, appearCurve, () => {
            transform.localScale = Vector3.one;
            ;
        }, appearDuration);
    }
}
