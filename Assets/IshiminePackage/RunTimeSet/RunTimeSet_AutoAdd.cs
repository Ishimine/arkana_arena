﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class RunTimeSet_AutoAdd<T> : SerializedMonoBehaviour {

    public RunTimeSet<T> set;
    public T elemento;
    

    public void OnEnable()
    {
        set.Add(elemento);
    }

    public void OnDisable()
    {
        set.Remove(elemento);
    }

}
