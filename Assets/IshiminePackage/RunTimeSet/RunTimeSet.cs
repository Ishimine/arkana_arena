﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public abstract class RunTimeSet<T> : SerializedScriptableObject {

    public List<T> Items = new List<T>();

    public void Add(T t)
    {
        if (!Items.Contains(t)) Items.Add(t);
    }

    public void Remove(T t)
    {
        if (Items.Contains(t)) Items.Remove(t);
    }

    public bool Contains(T t)
    {
        return Items.Contains(t);   
    } 


}
