﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;




[Serializable]
[CreateAssetMenu(fileName = "RunTimeSet_TrailObject", menuName = "RunTimeSet/RTS_TrailObject")]
public class RunTimeSet_TrailObject : RunTimeSet<TrailObject>
{

    public void AddById(GameObject nuevo, string id)
    {


        TrailObject t = Items.Find(x => x.id == id);

        if (t == null)
        {
            Add(new TrailObject(id, nuevo));
        }

    }


    public void RemoveById(string id)
    {
        Items.Remove(GetById(id));
    }

    public TrailObject GetById(string id)
    {
        return Items.Find(x => x.id == id);
    }



}


public class TrailObject
{
    public string id;
    public GameObject obj;

    public TrailObject(string id, GameObject obj)
    {
        this.id = id;
        this.obj = obj;
    }
}


