﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class RTSet_Collider2D_AutoAdd : RunTimeSet_AutoAdd<Collider2D> {

    private void OnValidate()
    {
        elemento = gameObject.GetComponent<Collider2D>();
    }
}
