﻿using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
public class ColorFromLineRenderer : ShowOdinSerializedPropertiesInInspectorAttribute, IColor
{
    public LineRenderer render;

    [SerializeField]
    public Color Value
    {
        get
        {
            return Color.Lerp(render.startColor, render.endColor,0.5f);
        }
        set
        {
        }
    }

    public float LocalAlpha
    {
        get
        {
            return 1;
        }
    }

    private bool useLocalAlpha = false;
    public bool UseLocalAlpha { get { return useLocalAlpha; } }
}
