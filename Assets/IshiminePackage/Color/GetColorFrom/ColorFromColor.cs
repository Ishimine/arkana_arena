﻿using System;
using UnityEngine;

public class ColorFromColor : IColor
{
    [SerializeField]
    private Color color;
    public Color Value
    {
        get
        {
            if (useLocalAlpha)
                color.a = localAlpha;
            return color;
        }

        set
        {
            color = value;
            if (useLocalAlpha)
                color.a = localAlpha;
        }
    }

    [SerializeField, Range(0,1)]
    private float localAlpha = 1;
    public float LocalAlpha
    {
        get
        {
            return localAlpha;
        }
    }

    [SerializeField]
    private bool useLocalAlpha = false;
    public bool UseLocalAlpha { get { return useLocalAlpha; } }
}
