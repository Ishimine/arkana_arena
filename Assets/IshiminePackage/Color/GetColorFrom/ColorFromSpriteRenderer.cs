﻿using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using System;

public class ColorFromSpriteRenderer : ShowOdinSerializedPropertiesInInspectorAttribute, IColor
{
    public SpriteRenderer imageRef;



    [SerializeField]
    public Color Value
    {
        get
        {
            if(useLocalAlpha)
            {
                return new Color(imageRef.color.r, imageRef.color.g, imageRef.color.b, localAlpha);
            }
            else
                return imageRef.color;
        }
        set
        {
        }
    }

    [SerializeField, Range(0, 1)]
    private float localAlpha = 1;
    public float LocalAlpha
    {
        get
        {
            return localAlpha;
        }
    }

    [SerializeField]
    private bool useLocalAlpha = false;
    public bool UseLocalAlpha { get { return useLocalAlpha; } }
}
