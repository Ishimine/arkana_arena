﻿using UnityEngine;

public class ColorFromVariable : IColor
{
    [SerializeField]
    private VariableColor color;
    private Color value;
    public Color Value
    {
        get
        {
            value = color.Value;
            if (useLocalAlpha)
                value.a = localAlpha;
            return value;
        }
        set
        {
            color.Value = value;
        }
    }
    [SerializeField, Range(0, 1)]
    private float localAlpha = 1;
    public float LocalAlpha
    {
        get
        {
            return localAlpha;
        }
    }
    [SerializeField]
    private bool useLocalAlpha = false;
    public bool UseLocalAlpha { get { return useLocalAlpha; } }

}
