﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IColor
{
    float LocalAlpha { get;}
    bool UseLocalAlpha { get;}
    Color Value { get; set; }
}
