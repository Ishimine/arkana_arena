﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXComp_V3_Particles : IFX_ComponentGeneric<Vector3>
{
    public ParticleSystem particles;

    public override void Activate(Vector3 var = default(Vector3))
    {
        particles.transform.position = var;
        particles.Play();
    }

    public override void Pause()
    {
    }

    public override void Stop()
    {
    }

}
