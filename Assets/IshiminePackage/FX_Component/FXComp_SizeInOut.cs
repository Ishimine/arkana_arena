﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXComp_SizeInOut : MonoBehaviour
{
    public DeltaTimeType delta;

    public float timeIn;
    public AnimationCurve curveIn;
    public float timeOut;
    public AnimationCurve curveOut;

    private CodeAnimator codeAnimator = new CodeAnimator();

    public void Activate(bool x)
    {
        if (x) Activate();
        else Deactivate();
    }
    public void Deactivate(bool x)
    {
        Activate(!x);
    }

    public void Activate()
    {
        gameObject.SetActive(true);
        codeAnimator.StartAnimacion(this,
            x =>
            {
                transform.localScale = x * Vector3.one;
            }, delta, AnimationType.Simple,curveIn, timeIn);
    }

    public void Deactivate()
    {
        if (gameObject.activeSelf == false)
            return;
        codeAnimator.StartAnimacion(this,
            x =>
            {
                transform.localScale = x * Vector3.one;
            }, delta, AnimationType.Simple, curveOut,()=> { gameObject.SetActive(false); }, timeOut);
    }

}
