﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class IFX_Component : SerializedMonoBehaviour
{
    [Button]
    public abstract void Activate();
    public abstract void Pause();
    public abstract void Stop();
}
