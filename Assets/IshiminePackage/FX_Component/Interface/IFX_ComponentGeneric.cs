﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IFX_ComponentGeneric<T> : MonoBehaviour
{
    public abstract void  Activate(T var);
    public abstract void Pause();
    public abstract void Stop();
}
