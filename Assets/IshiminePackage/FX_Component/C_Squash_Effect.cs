﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Collider2D))]
public class C_Squash_Effect : SerializedMonoBehaviour
{
    [Header("Animacion")]
    public bool usarPosDelay = false;
    bool aplicarDelay = false;
   

    [Range(0,1)]
    public float offset;
    [Range(0,1)]
    public float deformacion = .15f; 
    
    public float vel = 1;
    public float duracion = 3;
    public AnimationCurve curva = AnimationCurve.Linear(0,0,1,1);

    [Header("Dependencia")]
    public GameObject cPadre;
    public GameObject cHijo;
    public Rigidbody2D rb;

    IEnumerator rutina;


    public void AplicarDelay()
    {
        aplicarDelay = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        AplicarDelay();
        if (rutina != null)
        {            StopCoroutine(rutina);        }

        ContactPoint2D[] contacts = new ContactPoint2D[1];

        collision.GetContacts(contacts);
        StartSquash(contacts[0].point);
    }

    public void StartSquash(Vector3 point)
    {
        if (!gameObject.activeSelf) return;
        rutina = Squash(point);
        StartCoroutine(rutina);
    }


    public void OnDisable()
    {
        if (rutina != null) StopCoroutine(rutina);
    }

    IEnumerator Squash(Vector3 p)
    {
        float t = offset;
        float defAct = deformacion;
        float c = 0;
        do
        {
            t += (Time.deltaTime / duracion);
            //Rotacion
            Vector2 dif = cPadre.transform.position - p;
            float rot = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
            cPadre.transform.rotation = Quaternion.Euler(cPadre.transform.rotation.x, cPadre.transform.rotation.y, rot);
            cHijo.transform.rotation = cPadre.transform.parent.rotation;
            
            c = curva.Evaluate(t);
            float x = -c * deformacion + 1;
            float y = c * deformacion + 1;
            cPadre.transform.localScale = new Vector3(x, y,1);
 
            yield return null;
            if (usarPosDelay)
            {
                PosicionDelay();
            }
        } while (t < 1);
        cPadre.transform.localScale = new Vector3(1, 1,1);
    }


    public void PosicionDelay()
    {
        if (usarPosDelay && aplicarDelay)
        {
            transform.position = transform.position - (Vector3)rb.velocity/4*3 * Time.fixedDeltaTime;
            aplicarDelay = false;
        }
    }
}
