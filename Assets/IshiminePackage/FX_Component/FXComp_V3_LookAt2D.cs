﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXComp_V3_LookAt2D : IFX_ComponentGeneric<Vector3>
{
    [Range(0,1)]
    public float smoothTime;
    public float maxVelocity = 200;
    float vel;

    float anguloAct;
    float anguloTarget;

    public override void Activate(Vector3 var)
    {
        Vector3 dif = (var - transform.position).normalized;
        anguloTarget = Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
    }

    public override void Pause()
    {
    }

    public override void Stop()
    {
    }

    public void Update()
    {
        if (Mathf.Abs(anguloAct - anguloTarget) < .1f) return;
        anguloAct = Mathf.SmoothDamp(anguloAct, anguloTarget, ref vel, smoothTime, maxVelocity,Time.deltaTime);
        transform.rotation = Quaternion.Euler(0,0, anguloAct);
    }
}
