﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FX_Comp_Squash : MonoBehaviour
{
    public float duracion = 2;
    public float magnitud = 2;

    public AnimationCurve curva;

    IEnumerator rutina;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("FX_Comp_Squash");

        if (rutina != null) StopCoroutine(rutina);
        rutina = AnimacionSquash();
        StartCoroutine(rutina);
    }

    IEnumerator AnimacionSquash()
    {
        transform.localScale = Vector3.one;
        float t = 0;
        float c = 0;
        Vector2 nScale;
        do
        {
            t += Time.deltaTime / duracion;
            c = curva.Evaluate(t) * magnitud;

            nScale = new Vector3(1 - c, 1 + c, 1);

            transform.localScale = nScale;
            yield return null;
        } while (t < 1);
    }

}
