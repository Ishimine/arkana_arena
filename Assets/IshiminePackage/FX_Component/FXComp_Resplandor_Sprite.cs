﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FXComp_Resplandor_Sprite : IFX_Component
{
    public SpriteRenderer render;
    public IColor colorResplandor;
    public IColor colorFinal;

    public AnimationCurve curva;
   
    public float duracion;

    IEnumerator rutina;

    public override void Activate()
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = ResplandorAnim();
        StartCoroutine(rutina);
    }

    public override void Pause()
    {
    }

    public override void Stop()
    {
    }

    private void OnValidate()
    {
        if (render == null)
            render = GetComponent<SpriteRenderer>();
    }



    IEnumerator ResplandorAnim()
    {
        Color cInicial = colorResplandor.Value;
        Color cFinal = colorFinal.Value;
        Color cActual = render.color;
        float t = 0;
        float c = 0;
        do
        {
            t += Time.deltaTime / duracion;
            c = curva.Evaluate(t);
            cActual = Color.Lerp(cInicial, cFinal, c);
            render.color = cActual;
            yield return null;
        } while (t < 1);
        cActual = cFinal;
    }
}
