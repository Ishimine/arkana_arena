﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class QuickEventKeybordTester : SerializedMonoBehaviour
{
    public KeyCode key;

    public bool useStayDown = false;

    public bool usarBool = false;
    [ShowIf("usarBool")]
    public GameEventBool eventoBool;

    public bool usarVector3 = false;
    [ShowIf("usarVector3")]
    public GameEventVector3 eventoVector3;

    public bool usarVector2 = false;
    [ShowIf("usarVector2")]
    public GameEventVector2 eventoVector2;

    public bool usarFloat = false;
    [ShowIf("usarFloat")]
    public GameEventFloat eventoFloat;



    private void Update()
    {
        if (Input.GetKey(key) && useStayDown)
        {
            ExecuteEvent(eventoBool, true);
            ExecuteEvent(eventoVector3, Vector3.up);
            ExecuteEvent(eventoVector2, Vector3.up);
            ExecuteEvent(eventoFloat, 10);
        }
        else if (Input.GetKeyDown(key))
        {
            ExecuteEvent(eventoBool, true);
            ExecuteEvent(eventoVector3, Vector3.up);
            ExecuteEvent(eventoVector2, Vector3.up);
            ExecuteEvent(eventoFloat, 10);
        }
    }


    private void ExecuteEvent<T>(GameEventGeneric<T> ev, T var)
    {
        if (ev != null)
            ev.Raise(var);
    }
}
