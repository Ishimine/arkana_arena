﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

[Serializable]
public class TouchSimple  {

    public TouchPhase phase;
    public int fingerId;
    public Vector2 position;
    public Vector2 rawPosition;
    public Vector2 deltaPosition;
    public Vector2 worldPos;
    public float deltaTime;
    public int tapCount;
    public Vector3 posInicial;

    public TouchSimple(TouchPhase phase, int fingerId,Vector2 position)
    {
        this.phase = phase;
        this.fingerId = fingerId;
        this.position = position;
    //    this.rawPosition = rawPosition;
        worldPos = GetWorldPosition();
    }
    public TouchSimple(Touch t)
    {
        fingerId = t.fingerId;
        position = t.position;
        rawPosition = t.rawPosition;
        deltaPosition = t.deltaPosition;
        deltaTime = t.deltaTime;
        tapCount = t.tapCount;
        phase = t.phase;
        worldPos = GetWorldPosition();
    }

    public Touch ToTouch()
    {
        Touch t = new Touch();
        t.fingerId = fingerId;
        t.position = position;
        t.rawPosition = rawPosition;
        t.deltaPosition = deltaPosition;
        t.deltaTime = deltaTime;
        t.tapCount = tapCount;
        t.phase = phase;
        return t;
    }


    private Vector2 GetWorldPosition()
    {
        if (Camera.main == null) return Vector2.zero;
        return Camera.main.ScreenToWorldPoint(position) - new Vector3(0f, 0f, Camera.main.transform.position.z);
    }

}
