﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Sirenix.OdinInspector;

public class TouchReader : MonoBehaviour
{
    public GameManager gameManager;
    public Settings settings;

    public bool useUiFilter = true;
    public LayerMask layerMask;
    public bool leerMouse;
    public GameEventTouchSimple touchSimpleEvent;

    [SerializeField]
    private    GameObject lastGo;
    private CodeAnimator codeAnimator = new CodeAnimator();

    void Update()
    {
        if (!settings || gameManager.State.inPause) return;
        if (leerMouse)
        {
            if (useUiFilter)
            {
                if (EventSystem.current == null) return;
                if (lastGo != EventSystem.current.currentSelectedGameObject)
                    lastGo = EventSystem.current.currentSelectedGameObject;
            }

            if (lastGo != null && layerMask == (layerMask | (1 << lastGo.layer)))
            {
                return;
            }
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                
                TouchSimple nT = new TouchSimple(TouchPhase.Began,0, Input.mousePosition);
                touchSimpleEvent.Raise(nT);
            }
            else if(Input.GetMouseButton(0))
            {
                 TouchSimple nT = new TouchSimple(TouchPhase.Moved, 0, Input.mousePosition);
                touchSimpleEvent.Raise(nT);
            }
            else if(Input.GetMouseButtonUp(0))
            {
                 TouchSimple nT = new TouchSimple(TouchPhase.Ended, 0, Input.mousePosition);
                touchSimpleEvent.Raise(nT);
            }
#endif
        }

#if UNITY_ANDROID

        foreach (Touch t in Input.touches)
        {
            Debug.Log(t);
            TouchSimple nT = new TouchSimple(t);
            if (useUiFilter && t.phase == TouchPhase.Began)
            {
                if (EventSystem.current.IsPointerOverGameObject(t.fingerId))
                {
                    Debug.Log("Clicked on the UI: ");
                    continue;
                }
            }
            touchSimpleEvent.Raise(nT);
        }
#endif
    }
}
