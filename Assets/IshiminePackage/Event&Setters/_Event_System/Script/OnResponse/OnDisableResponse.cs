﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnDisableResponse : MonoBehaviour {

    public UnityEvent respuesta;

    private void OnDisable()
    {
        respuesta.Invoke();
    }
}
