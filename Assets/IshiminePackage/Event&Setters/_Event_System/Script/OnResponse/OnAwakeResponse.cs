﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnAwakeResponse : MonoBehaviour
{
    public UnityEvent response;

    private void Awake()
    {
        if (response != null)
            response.Invoke();
    }


}
