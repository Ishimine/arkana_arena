﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
public class OnCollision2DResponse : MonoBehaviour {

    public GameEventCollision2D evento;

    public bool onEnter = true;
    public bool onStay = false;
    public bool onExit = false;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (onEnter)
            evento.Raise(collision);
    }


    private void OnCollisionStay2D(Collision2D collision)
    {
        if (onStay)
            evento.Raise(collision);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (onExit)
            evento.Raise(collision);
    }





}
