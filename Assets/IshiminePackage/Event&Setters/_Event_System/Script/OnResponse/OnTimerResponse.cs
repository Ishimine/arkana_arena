﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTimerResponse : MonoBehaviour
{
    private bool activado;
    public bool Activado
    {
        get
        {
            return activado;
        }
    }

    public bool ciclico = true;
    public bool playOnAwake = false;
    public bool playOnEnable = false;
    public float tiempo;

    IEnumerator rutina;
    public UnityEvent respuesta;

    private void Awake()
    {
        if (playOnAwake)
            IniciarCuentaRegresiva();
    }

    public void Activar()
    {
        IniciarCuentaRegresiva();
    }

    public void OnEnable()
    {
        if (playOnEnable)
            IniciarCuentaRegresiva();
    }
    public void DetenerCuenta()
    {
        if (rutina != null) StopCoroutine(rutina);
    }

    public void IniciarCuentaRegresiva()
    {
        activado = true;
        if (rutina != null) StopCoroutine(rutina);
        rutina = CuentaRegresiva();
        StartCoroutine(rutina);
    }

    IEnumerator CuentaRegresiva()
    {
        yield return new WaitForSeconds(tiempo);
        if (ciclico)
            IniciarCuentaRegresiva();
        else
            activado = false;
        if (respuesta != null)
            respuesta.Invoke();
    }


    private void OnDisable()
    {
        activado = false;
        if (rutina != null) StopCoroutine(rutina);
    }
}
