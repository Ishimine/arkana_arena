﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEnableResponse : MonoBehaviour {

    public UnityEvent respuesta;

    private void OnEnable()
    {
        respuesta.Invoke();
    }



}
