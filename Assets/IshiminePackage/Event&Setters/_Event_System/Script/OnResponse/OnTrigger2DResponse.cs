﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Collider2D))]
public class OnTrigger2DResponse : SerializedMonoBehaviour
{
    public bool useTagFilter;
    [ShowIf("useTagFilter")]
    public string tagFilter = "Player";

    public GameEventCollider2D evento;

    public bool onEnter = true;
    public bool onStay = false;
    public bool onExit = false;

    public UnityEventCollider2D response;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (useTagFilter && other.tag != tagFilter)
            return;

//        Debug.Log(gameObject.name + " trigger with " + other.gameObject.name);

        if (onEnter)
        {
            if (evento != null)
                evento.Raise(other);
            if (response != null)
                response.Invoke(other);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (useTagFilter && other.tag != tagFilter)
            return;

        if (onStay)
        { 
            if (evento != null)
                evento.Raise(other);
            if (response != null)
                response.Invoke(other);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (useTagFilter && other.tag != tagFilter)
            return;

        if (onExit)
        {
            if (evento != null)
                evento.Raise(other);
            if (response != null)
                response.Invoke(other);
        }
    }





}
