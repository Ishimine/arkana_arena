﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "bool_", menuName = "Variables/bool")]
public class VariableBool : Variable<bool> {
}
