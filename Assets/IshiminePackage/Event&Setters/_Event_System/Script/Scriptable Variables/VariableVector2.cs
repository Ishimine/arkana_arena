﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "Vector2_", menuName = "Variables/Vector2")]
public class VariableVector2 : Variable<Vector2> {
}
