﻿
using UnityEngine;
using System;
using Sirenix.OdinInspector;

[Serializable]
[CreateAssetMenu(fileName = "Touch_", menuName = "Variables/Touch")]
public class VariableTouch : Variable<TouchSimple> {
}
