﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "GameObject_", menuName = "Variables/GameObject")]
public class VariableGameObject : Variable<GameObject> {
}
