﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "float_", menuName = "Variables/float")]
public class VariableFloat : Variable<float> {
}
