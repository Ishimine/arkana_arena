﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System;


[Serializable]
public class Variable<T> : SerializedScriptableObject, IVariable<T>
{
    [OnValueChanged("OnEnable")]
    public T DefaultValue;

   [SerializeField] private T actual;

    public T Value
    {
        get { return actual; }
        set { actual = value; }
    }


    private void OnEnable()
    {
        RestartValue();
    }

    public void RestartValue()
    {
        actual = DefaultValue;
    }

    
}
