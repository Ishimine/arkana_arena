﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "int_", menuName = "Variables/int")]
public class VariableInt : Variable<int> {
}
