﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "Vector4_", menuName = "Variables/Vector4")]
public class VariableVector4 : Variable<Vector4> {
}
