﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "direccion", menuName = "Variables/enum_Direccion")]
public class VariableDireccion : Variable<Direccion> {
}


public enum Direccion { Arriba_Abajo, Abajo_Arriba, Izquierda_Derecha, Derecha_Izquierda }
