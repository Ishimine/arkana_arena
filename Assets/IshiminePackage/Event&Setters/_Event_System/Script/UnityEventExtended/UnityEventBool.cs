﻿using UnityEngine.Events;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class UnityEventBool : UnityEvent<bool>
{

}
