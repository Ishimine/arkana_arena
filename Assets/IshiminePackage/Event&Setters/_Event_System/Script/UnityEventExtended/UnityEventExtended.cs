﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;


[System.Serializable]
public class UnityEventExtended<T> : UnityEvent<T>
{
    public T usarVarExterna;
    [HideIf("usarVarExterna")]
    public Variable<T> variable;


    public void InvokeExtended()
    {
        if (variable != null)
            base.Invoke(variable.Value);
        else
            Debug.LogError("Variable generica de referencia sin asignacion. Error Fatal, move el orto y arreglalo ahora...");
    }

}
