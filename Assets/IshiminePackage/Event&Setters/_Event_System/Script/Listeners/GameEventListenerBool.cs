﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using System;

public class GameEventListenerBool : GameEventListenerGeneric<bool>, IGameEventListenerGeneric<bool>
{

    public GameEventBool[] eventos;
    public override GameEventGeneric<bool>[] Eventos
    {
        get
        {
            return eventos;
        }
    }

    public UnityEventBool response;
    public override UnityEvent<bool> Response
    {
        get { return response; }
        //set { response = (UnityEventBool)value; }
    }

    [ShowIf("usarIfElse")]
    public UnityEventBool responseFalse;
    public override UnityEvent<bool> ResponseFalse
    {
        get { return responseFalse; }
     //   set => throw new NotImplementedException();
    }
}
