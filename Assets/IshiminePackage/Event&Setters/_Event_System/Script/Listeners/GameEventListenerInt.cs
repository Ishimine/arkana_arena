﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[System.Serializable]
public class GameEventListenerInt : GameEventListenerGeneric<int>
{
    public GameEventInt[] eventos;
    public override GameEventGeneric<int>[] Eventos
    {
        get
        {
            return eventos;
        }
    }
    public UnityEventInt response;
    public override UnityEvent<int> Response
    {
        get { return response; }
        //set { response = (UnityEventBool)value; }
    }

    [ShowIf("usarIfElse")]

    public UnityEventInt responseFalse;
    public override UnityEvent<int> ResponseFalse
    {
        get { return responseFalse; }
        //   set => throw new NotImplementedException();
    }
}

