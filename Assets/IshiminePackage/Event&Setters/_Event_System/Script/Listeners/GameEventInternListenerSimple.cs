﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class GameEventInternListenerSimple : ShowOdinSerializedPropertiesInInspectorAttribute, IGameEventListener
{
    public GameEvent[] eventos;
    public virtual GameEvent[] Eventos
    { get { return eventos; } set { eventos = value; } }

    public delegate void EventoRespuesta();
    public EventoRespuesta Respuesta;

    public GameEventInternListenerSimple()
    {
        Eventos = null;
    }

    public GameEventInternListenerSimple(GameEvent evento, EventoRespuesta respuesta)
    {
        Eventos = new GameEvent[1];
        Eventos[0] = evento;
        Respuesta -= respuesta;
        Respuesta += respuesta;
        OnEnable();
    }

    public GameEventInternListenerSimple(GameEvent[] events, EventoRespuesta respuesta)
    {
        Eventos = events;
        Respuesta -= respuesta;
        Respuesta += respuesta;
        OnEnable();
    }

    public GameEventInternListenerSimple(GameEvent[] events, EventoRespuesta[] respuestas)
    {
        Eventos = events;
        foreach(EventoRespuesta er in respuestas)
        {
            Respuesta -= er;
            Respuesta += er;
        }
        OnEnable();
    }

    public void Replace(GameEvent evento, EventoRespuesta respuesta, int id = 0)
    {
        if(Eventos == null || Eventos.Length == 0 )
            Eventos = new GameEvent[1];
        Eventos[id] = evento;
        Respuesta -= respuesta;
        Respuesta += respuesta;
        OnEnable();
    }

    /// <summary>
    /// No recomendable. Agrega un metodo al evento de respuesta
    /// </summary>
    /// <param name="action"></param>
    public void Add(EventoRespuesta action)
    {
        Respuesta -= action;
        Respuesta += action;
    }


    /// <summary>
    /// No recomendable. Remueve un metodo del evento de respuesta
    /// </summary>
    /// <param name="action"></param>
    public void Remove(EventoRespuesta action)
    {
        Respuesta -= action;
    }

    /// <summary>
    /// DEBE ser llamado desde el MonoBehaviour.OnEnable() contenedor
    /// </summary>
    public virtual void OnEnable()
    {
        if (Eventos == null) return;
        foreach (GameEvent evento in Eventos)
        {
            if(evento != null)            evento.RegisterListener(this);
        }
    }

    /// <summary>
    /// DEBE ser llamado desde el MonoBehaviour.OnDisable() contenedor
    /// </summary>
    public virtual void OnDisable()
    {
        if (Eventos == null) return;
        foreach (GameEvent evento in Eventos)
        {
            if (evento != null)                evento.UnregisterListener(this);
        }
    }


    [Button]
    /// <summary>
    /// Llamado por los GameEvent<T> eventos
    /// </summary>
    /// <param name="var"></param>
    public virtual void OnEventRaised()
    {
      //  Debug.Log("Raised");
        if (Respuesta != null)
        {
//            Debug.Log("RESPUESTA");
            Respuesta.Invoke();
        }
    }

}
