﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameEventInternListener<T> :  IGameEventListenerGeneric<T>
{
    public GameEventGeneric<T>[] eventos;

    public virtual GameEventGeneric<T>[] Eventos
    { get { return eventos; } set { eventos = value; } }

    public delegate void EventoRespuesta(T valor);
    private EventoRespuesta Respuesta;

    public GameEventInternListener()
    {
        Eventos = null;
    }

    public void Replace(GameEventGeneric<T> evento, EventoRespuesta respuesta, int id = 0)
    {
        if (Eventos == null || Eventos.Length == 0)
            Eventos = new GameEventGeneric<T>[1];
        Eventos[id] = evento;
        Respuesta -= respuesta;
        Respuesta += respuesta;
        OnEnable();
    }

    public GameEventInternListener(GameEventGeneric<T> events, EventoRespuesta respuesta)
    {
        Eventos = new GameEventGeneric<T>[1];
        Eventos[0] = events;
        Respuesta -= respuesta;
        Respuesta += respuesta;
        OnEnable();
    }

    public GameEventInternListener(GameEventGeneric<T>[] events, EventoRespuesta respuesta)
    {
        Eventos = events;
        Respuesta -= respuesta;
        Respuesta += respuesta;
        OnEnable();
    }

    public GameEventInternListener (GameEventGeneric<T>[] events, EventoRespuesta[] respuestas)
    {
        Eventos = events;
        foreach(EventoRespuesta er in respuestas)
        {
            Respuesta -= er;
            Respuesta += er;
        }
        OnEnable();
    }

    /// <summary>
    /// No recomendable. Agrega un metodo al evento de respuesta
    /// </summary>
    /// <param name="action"></param>
    public void Add(EventoRespuesta action)
    {
        Respuesta -= action;
        Respuesta += action;
    }

    /// <summary>
    /// No recomendable. Remueve un metodo del evento de respuesta
    /// </summary>
    /// <param name="action"></param>
    public void Remove(EventoRespuesta action)
    {
        Respuesta -= action;
    }

    /// <summary>
    /// DEBE ser llamado desde el MonoBehaviour.OnEnable() contenedor
    /// </summary>
    public virtual void OnEnable()
    {
        if (Eventos == null) return;
        foreach (GameEventGeneric<T> evento in Eventos)
        {
            if(evento != null)            evento.RegisterListener(this);
        }
    }

    /// <summary>
    /// DEBE ser llamado desde el MonoBehaviour.OnDisable() contenedor
    /// </summary>
    public virtual void OnDisable()
    {
        if (Eventos == null) return;
        foreach (GameEventGeneric<T> evento in Eventos)
        {
            if(evento != null)            evento.UnregisterListener(this);
        }
    }


    /// <summary>
    /// Llamado por los GameEvent<T> eventos
    /// </summary>
    /// <param name="var"></param>
    public virtual void OnEventRaised(T var)
    {
        if (Respuesta != null)
            Respuesta.Invoke(var);
    }

}
