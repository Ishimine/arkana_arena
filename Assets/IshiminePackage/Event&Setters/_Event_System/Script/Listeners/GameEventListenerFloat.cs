﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[System.Serializable]
public class GameEventListenerFloat : GameEventListenerGeneric<float>
{
    public GameEventFloat[] eventos;
    public override GameEventGeneric<float>[] Eventos
    {
        get
        {
            return eventos;
        }
    }

    public UnityEventFloat response;
    public override UnityEvent<float> Response
    {
        get { return response; }
        //set { response = (UnityEventBool)value; }
    }

    [ShowIf("usarIfElse")]
    public UnityEventFloat responseFalse;
    public override UnityEvent<float> ResponseFalse
    {
        get { return responseFalse; }
        //   set => throw new NotImplementedException();
    }
}

