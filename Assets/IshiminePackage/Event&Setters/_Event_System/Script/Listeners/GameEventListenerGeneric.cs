﻿using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[System.Serializable]
public abstract class GameEventListenerGeneric<T> : SerializedMonoBehaviour, IGameEventListenerGeneric<T>
{
    bool isActive = false;

    public bool usarIfElse;
    

    public abstract GameEventGeneric<T>[] Eventos
    {
        get;
    }

    public abstract UnityEvent<T> Response
    {
        get; // set;
    }

    [ShowIf("usarIfElse")]
    public abstract UnityEvent<T> ResponseFalse
    {
        get; //set;
    }
    [ShowIf("usarIfElse")]
   public bool usarBooleanExterno;

   [ShowIf("usarIfElse")]
   [ShowIf("usarBooleanExterno")]
   [SerializeField]
   private VariableBool booleanoExterno;

   [ShowIf("usarIfElse")]
   [HideIf("usarBooleanExterno")]
   [SerializeField]
   private bool booleano;

   public bool Value
   {
       get
       {
           if (usarBooleanExterno)
               return booleanoExterno.Value;
           else
               return booleano;
       }
       set
       {
           booleano = value;
       }
   }

   private void Awake()
   {
       isActive = true;
   }
    

   public virtual void OnEnable()
   {
       if (!isActive) return;

       foreach (GameEventGeneric<T> evento in Eventos)
       {
            if(evento != null)           evento.RegisterListener(this);
       }
   }

   public virtual void OnDisable()
   {
       foreach (GameEventGeneric<T> evento in Eventos)
       {
            if(evento != null)           evento.UnregisterListener(this);
        }
   }

   public virtual void OnEventRaised(T var)
   {
       if (!isActive) return;

       if (usarIfElse)
       {
           if (booleano)
           {
               if (Response != null)
                   Response.Invoke(var);
           }
           else
           {
               if (ResponseFalse != null)
                   ResponseFalse.Invoke(var);
           }
       }
       else if (Response != null)
           Response.Invoke(var);
   }

}
