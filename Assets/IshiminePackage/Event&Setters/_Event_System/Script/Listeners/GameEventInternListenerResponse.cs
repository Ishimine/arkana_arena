﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventInternListenerResponse<T> : IGameEventListenerGeneric<T>
{
    public GameEventGeneric<T>[] eventos;

    public virtual GameEventGeneric<T>[] Eventos
    { get { return eventos; } set { eventos = value; } }

    [SerializeField]
    private UnityEvent<T> Respuesta;

    public GameEventInternListenerResponse()
    {
        Eventos = null;
    }

    /// <summary>
    /// DEBE ser llamado desde el MonoBehaviour.OnEnable() contenedor
    /// </summary>
    public virtual void OnEnable()
    {
        if (Eventos == null) return;
        foreach (GameEventGeneric<T> evento in Eventos)
        {
            if (evento != null) evento.RegisterListener(this);
        }
    }

    /// <summary>
    /// DEBE ser llamado desde el MonoBehaviour.OnDisable() contenedor
    /// </summary>
    public virtual void OnDisable()
    {
        if (Eventos == null) return;
        foreach (GameEventGeneric<T> evento in Eventos)
        {
            if (evento != null) evento.UnregisterListener(this);
        }
    }


    /// <summary>
    /// Llamado por los GameEvent<T> eventos
    /// </summary>
    /// <param name="var"></param>
    public virtual void OnEventRaised(T var)
    {
        if (Respuesta != null)
            Respuesta.Invoke(var);
    }

}