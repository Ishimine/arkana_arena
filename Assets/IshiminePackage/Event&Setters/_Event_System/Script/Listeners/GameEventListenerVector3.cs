﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[System.Serializable]
public class GameEventListenerVector3 : GameEventListenerGeneric<Vector3>
{
    public GameEventVector3[] eventos;
    public override GameEventGeneric<Vector3>[] Eventos
    {
        get
        {
            return eventos;
        }
    }

    public UnityEventVector3 response;
    public override UnityEvent<Vector3> Response
    {
        get { return response; }
        //set { response = (UnityEventBool)value; }
    }

    [ShowIf("usarIfElse")]

    public UnityEventVector3 responseFalse;
    public override UnityEvent<Vector3> ResponseFalse
    {
        get { return responseFalse; }
        //   set => throw new NotImplementedException();
    }
}

