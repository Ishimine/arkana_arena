﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using Sirenix.OdinInspector;
[System.Serializable]
public class GameEventListenerTouch : GameEventListenerGeneric<TouchSimple>
{
    public GameEventTouchSimple[] eventos;
    public override GameEventGeneric<TouchSimple>[] Eventos
    {
        get
        {
            return eventos;
        }
    }

    public UnityEventTouchSimple response;
    public override UnityEvent<TouchSimple> Response
    {
        get { return response; }
        //set { response = (UnityEventBool)value; }
    }

    [ShowIf("usarIfElse")]

    public UnityEventTouchSimple responseFalse;
    public override UnityEvent<TouchSimple> ResponseFalse
    {
        get { return responseFalse; }
        //   set => throw new NotImplementedException();
    }
}

