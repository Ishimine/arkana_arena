﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using System;

[System.Serializable]
public class GameEventListener : SerializedMonoBehaviour, IGameEventListener
{
    protected bool isActive = false;

    public bool usarIfElse;

    public bool ocultarEventos;
    [HideIf("ocultarEventos")]
    public GameEvent[] Eventos;

    public bool ocultarRespuesta;
    [HideIf("ocultarRespuesta")]
    public UnityEvent Response;

    [ShowIf("usarIfElse")]
    [HideIf("ocultarRespuesta")]
    public UnityEvent ResponseFalse;

    [ShowIf("usarIfElse")]
    public bool usarBooleanExterno;

    [ShowIf("usarIfElse")]
    [ShowIf("usarBooleanExterno")]
    [SerializeField]
    private IVariable<bool> booleanoExterno;

    [ShowIf("usarIfElse")]
    [HideIf("usarBooleanExterno")]
    [SerializeField]
    protected bool booleano;


    public bool Value
    {
        get
        {
            if (usarBooleanExterno)
                return booleanoExterno.Value;
            else
                return booleano;
        }
        set
        {
            booleano = value;
        }
    }

    private void Awake()
    {
        isActive = true;
    }

    public virtual void OnEnable()
    {
        if (!isActive) return;

      //  Debug.Log(gameObject.name);
        foreach (GameEvent evento in Eventos)
        {
            if(evento != null)
                evento.RegisterListener(this);
        }
    }

    public virtual void OnDisable()
    {
        foreach (GameEvent evento in Eventos)
        {
            if(evento != null)
                evento.UnregisterListener(this);
        }
    }

    [Button]
    public virtual void OnEventRaised()
    {
        if (!isActive) return;

        if (usarIfElse)
        {
       //     Debug.Log("A");
            if (Value)
            {
        //    Debug.Log("B");
                if (Response != null)
                    Response.Invoke();
            }
            else
            {
         //   Debug.Log("C");
                if (ResponseFalse != null)
                    ResponseFalse.Invoke();
            }
        }
        else if (Response != null)
            Response.Invoke();
    }

}
