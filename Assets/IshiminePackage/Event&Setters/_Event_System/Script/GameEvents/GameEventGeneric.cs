﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public abstract class GameEventGeneric<T> : SerializedScriptableObject, IVariable<T>
{
    [SerializeField]
    private T value = default(T);
    public T Value
    {
        get
        {
            return value;
        }
        set
        {
            this.value = value;
        }
    }

    public void OnEnable()
    {
        listeners = new List<IGameEventListenerGeneric<T>>();
    }

    [SerializeField]
    private List<IGameEventListenerGeneric<T>> listeners = null;
    public void Raise(T var)
    {
        Value = var;
        Raise();
    }

    [Button]
    public void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(Value);
        }
    }

    public  void RegisterListener(IGameEventListenerGeneric<T> listener)
    {
       // if (listeners == null) listeners = new List<IGameEventListenerGeneric<T>>();
        if(!listeners.Contains(listener))
            listeners.Add(listener);
    }

    public  void UnregisterListener(IGameEventListenerGeneric<T> listener)
    {
        listeners.Remove(listener);
    }


}
