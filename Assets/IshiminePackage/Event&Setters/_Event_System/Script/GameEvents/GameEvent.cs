﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;


[ExecuteInEditMode]
[Serializable]
[CreateAssetMenu(menuName = "GameEvents/Simple",fileName = "eSimple")]
public class GameEvent : SerializedScriptableObject
{
    [SerializeField]
    private List<IGameEventListener> listeners = null;

    [Button]
    public virtual void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised();
        }
    }

    public void OnEnable()
    {
        listeners = new List<IGameEventListener>();
    }

    public void RegisterListener(IGameEventListener listener)
    {
        if(!listeners.Contains(listener))
            listeners.Add(listener);
    }

    public void UnregisterListener(IGameEventListener listener)
    {
        listeners.Remove(listener);
    }

}
