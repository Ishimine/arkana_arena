﻿using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
[CreateAssetMenu(menuName = "GameEvents/TouchSimple",fileName = "eTouchSimple")]
public class GameEventTouchSimple : GameEventGeneric<TouchSimple>
{

}
