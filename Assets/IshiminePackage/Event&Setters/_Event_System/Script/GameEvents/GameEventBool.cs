﻿using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
[CreateAssetMenu(menuName = "GameEvents/Bool",fileName = "eBool")]
public class GameEventBool: GameEventGeneric<bool>
{

}
