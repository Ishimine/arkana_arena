﻿using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
[CreateAssetMenu(menuName = "GameEvents/Collider2D", fileName = "eCollider2D")]
public class GameEventCollider2D : GameEventGeneric<Collider2D>
{

}
