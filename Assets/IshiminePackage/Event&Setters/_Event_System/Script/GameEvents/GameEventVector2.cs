﻿using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(menuName = "GameEvents/Vector2",fileName = "eVector2")]
public class GameEventVector2 : GameEventGeneric<Vector2>
{

}
