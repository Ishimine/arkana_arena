﻿using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
[CreateAssetMenu(menuName = "GameEvents/Collision2D", fileName = "eCollision2D")]
public class GameEventCollision2D : GameEventGeneric<Collision2D>
{

}
