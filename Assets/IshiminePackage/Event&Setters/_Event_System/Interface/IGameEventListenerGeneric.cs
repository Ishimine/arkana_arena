﻿

public interface IGameEventListenerGeneric<T>
{
    void OnEventRaised(T var);
}
