﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetV2FromTouch : SetVector2Base
{
    public GameEventTouchSimple touchEvent;
    private GameEventInternListener<TouchSimple> listener;

    /// <summary>
    /// Inicializacion
    /// </summary>
    /// <param name="root">Elemento en base al cual se calculara la "direccion" seteada</param>
    /// <param name="setDirFunct">Funcion que se enlazara al evento de SetVector2</param>
    public override void Initialize(Transform root, SetDirEvent setDirFunct)
    {
        base.Initialize(root, setDirFunct);
        listener = new GameEventInternListener<TouchSimple>(touchEvent, ReadTouch);
    }

    private void ReadTouch(TouchSimple t)
    {
        SetDirectionFrom(t.worldPos);
    }

    public override void OnDisable()
    {
        listener.OnDisable();
    }

    public override void OnEnable()
    {
        listener.OnEnable();
    }

    public override void Update()
    {
    }
}
