﻿using UnityEngine;
public interface ISetVector2
{
    void SetVector2(Vector2 dir);
    void OnEnable();
    void OnDisable();
}
