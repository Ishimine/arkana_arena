﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetV2FromVector2Event : SetVector2Base
{
    public GameEventVector2 vector2Event;
    public GameEventInternListener<Vector2> listener;

    /// <summary>
    /// Inicializacion
    /// </summary>
    /// <param name="root">Elemento en base al cual se calculara la "direccion" seteada</param>
    /// <param name="setDirFunct">Funcion que se enlazara al evento de SetVector2</param>
    public override void Initialize(Transform root, SetDirEvent setDirFunct)
    {
        base.Initialize(root, setDirFunct);
        listener = new GameEventInternListener<Vector2>(vector2Event, SetDirectionFrom);
    }


    public override void OnDisable()
    {
        listener.OnDisable();
    }

    public override void OnEnable()
    {
        listener.OnEnable();
    }

    public override void Update()
    {
    }
}
