﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetV2FromSwipe : SetVector2Base
{
    public GameEventTouchSimple touchEvent;
    GameEventInternListener<TouchSimple> listener;

    private Vector2 posInicial;

    /// <summary>
    /// Inicializacion
    /// </summary>
    /// <param name="root">Elemento en base al cual se calculara la "direccion" seteada</param>
    /// <param name="setDirFunct">Funcion que se enlazara al evento de SetVector2</param>
    public override void Initialize(Transform root, SetDirEvent setDirFunct)
    {
        base.Initialize(root, setDirFunct);
        listener = new GameEventInternListener<TouchSimple>(touchEvent, ReadTouch);
    }

    private void ReadTouch(TouchSimple t)
    {
        switch (t.phase)
        {
            case TouchPhase.Began:
                posInicial = t.position;
                break;
            case TouchPhase.Moved:
                SetVector2(t.position - posInicial);
//                Debug.Log("t.position - posInicial: " + (t.position - posInicial));
                break;
            case TouchPhase.Stationary:
                SetVector2(t.position - posInicial);
                break;
            case TouchPhase.Ended:
                SetVector2(Vector2.zero);
                break;
            case TouchPhase.Canceled:
                SetVector2(Vector2.zero);
                break;
            default:
                break;
        }
    }

    public override void SetVector2(Vector2 dir)
    {
        dir /= 100;
        if (evento != null)
            evento(dir * InverseValue);
    }

    public override void OnDisable()
    {
        listener.OnDisable();
    }

    public override void OnEnable()
    {
        listener.OnEnable();
    }

    public override void Update()
    {
    }
}
