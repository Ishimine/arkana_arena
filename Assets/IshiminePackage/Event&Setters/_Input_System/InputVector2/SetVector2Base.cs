﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SetVector2Base : ISetVector2 
{
    private Transform root;
    public delegate void SetDirEvent(Vector2 dir);
    protected SetDirEvent evento;

    public bool inverse;
    protected int InverseValue
    {
        get
        {
            if (inverse) return -1;
            else return 1;
        }
    }

    /// <summary>
    /// Inicializacion
    /// </summary>
    /// <param name="root">Elemento en base al cual se calculara la "direccion" seteada</param>
    /// <param name="setDirFunct">Funcion que se enlazara al evento de SetVector2</param>
    public virtual void Initialize(Transform root, SetDirEvent setDirFunct)
    {
        this.root = root;
        evento = setDirFunct;
    }
    
    public virtual void SetVector2(Vector2 dir)
    {
//        Debug.Log("dir: " + dir);

        if (evento != null)
            evento(dir * InverseValue);
    }

    public virtual void SetDirectionFrom(Vector2 otherPos)
    {
        SetVector2(otherPos - (Vector2)root.position);
    }

    public abstract void Update();
    public abstract void OnEnable();
    public abstract void OnDisable();
}
