﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class SetTriggerFromTouch : SetTriggerBase
{
    public GameEventTouchSimple touchEvento;
    public TouchPhase touchPhaseTrigger;
    private GameEventInternListener<TouchSimple> listener;

    void ReadTouch(TouchSimple t)
    {
        if (t.phase == touchPhaseTrigger)
            Execute(true);
    }

    public override void Initialize(eventInput injectedFuntion)
    {
        base.Initialize(injectedFuntion);
        listener = new GameEventInternListener<TouchSimple>(touchEvento, ReadTouch);
    }

    public override void OnDisable()
    {
        listener.OnDisable();
    }
    public override void OnEnable()
    {
        listener.OnEnable();
    }

    public override void Update()
    {
    }
}
