﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SetTriggerFromEvent : SetTriggerBase
{
    public bool triggerWithTrueOnly = true;
    public GameEventBool eventoBool;
    private GameEventInternListener<bool> listener;

    public override void Initialize(eventInput injectedFuntion)
    {
        base.Initialize(injectedFuntion);
        listener = new GameEventInternListener<bool>(eventoBool, Execute);
    }

    public override void OnDisable()
    {
        listener.OnDisable();
    }
    public override void OnEnable()
    {
        listener.OnEnable();
    }
    public override void Update()
    {
    }
}
