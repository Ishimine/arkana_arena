﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISetTrigger
{
    void Execute();
    void OnEnable();
    void OnDisable();
}
