﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SetTriggerFromKeycode : SetTriggerBase
{
    public KeyCode key;

    /// <summary>
    /// Is pushed
    /// </summary>
    public bool onGetKeyDown;
    /// <summary>
    /// is Held Down
    /// </summary>
    public bool onGetKey;
    /// <summary>
    /// is released
    /// </summary>
    public bool onGetKeyUp;

    public override void Initialize(eventInput injectedFuntion)
    {
        base.Initialize(injectedFuntion);
    }

    public override void OnDisable()
    {
    }

    public override void OnEnable()
    {
    }

    public override void Update()
    {
        if (onGetKeyDown && Input.GetKeyDown(key)) Execute();
        if (onGetKey && Input.GetKey(key)) Execute();
        if (onGetKeyUp && Input.GetKeyUp(key)) Execute();
    }
}
