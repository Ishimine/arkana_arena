﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class SetTriggerBase : InputGeneric<bool>
{
    public override void Initialize(eventInput injectedFuntion)
    {
        base.Initialize(injectedFuntion);
    }
}
