﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputSetter
{
    void Execute();
    void OnEnable();
    void OnDisable();
    void Update();
}
