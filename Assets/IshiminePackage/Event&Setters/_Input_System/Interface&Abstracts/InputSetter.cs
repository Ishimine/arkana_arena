﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputSetter: IInputSetter
{
    public delegate void eventInput();
    private eventInput evento;

    /// <summary>
    /// Ejecuta el evento interno devolviendo el elemento pasado como "retValue"
    /// </summary>
    /// <param name="dir"></param>
    public virtual void Execute() 
    {
        if (evento != null)
            evento.Invoke();
    }

    public virtual void Initialize(eventInput injectedFuntion)
    {
        evento = injectedFuntion;
    }

    public abstract void OnDisable();

    public abstract void OnEnable();

    public abstract void Update();

    public void Register(eventInput e)
    {
        evento -= e;
        evento += e;
    }

    public void Unregister(eventInput e)
    {
        evento -= e;
    }
}
