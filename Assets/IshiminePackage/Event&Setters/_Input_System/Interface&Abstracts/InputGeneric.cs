﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputGeneric<T> : IInputSetter
{
    public T Value {get; private set;}
    public delegate void eventInput(T input);
    private eventInput evento;

    /// <summary>
    /// Ejecuta el evento interno devolviendo el elemento pasado como "retValue"
    /// </summary>
    /// <param name="dir"></param>
    public virtual void Execute() 
    {
        if (evento != null)
            evento.Invoke(Value);
    }

    public virtual void Execute(T nValue)
    {
        Value = nValue;
        Execute();
    }

    public virtual void Initialize(eventInput injectedFuntion)
    {
        evento = injectedFuntion;
    }

    public abstract void OnDisable();

    public abstract void OnEnable();

    public abstract void Update();

    public virtual void Register(eventInput e)
    {
        evento -= e;
        evento += e;
    }

    public virtual void Unregister(eventInput e)
    {
        evento -= e;
    }
}
