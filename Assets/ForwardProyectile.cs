﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardProyectile : Proyectile
{
    public Rigidbody2D rb;


    public override void Activate()
    {
        base.Activate();
        active = true;
    }

  

    public virtual void FixedUpdate()
    {
        if (Active)
            rb.MovePosition(transform.position + transform.up * speed * Time.fixedDeltaTime);
            //transform.Translate(Vector3.up * speed * Time.fixedDeltaTime);
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        IHealth h = collision.gameObject.GetComponent<IHealth>();
        IUseGrid g = collision.gameObject.GetComponent<IUseGrid>();
        if (h != null)
        {
            h.GetDamage(damage);
        }

        if (g != null)
        {
            ContactPoint2D[] contacts = new ContactPoint2D[1];
            collision.GetContacts(contacts);
            g.KnockBack(contacts[0].point, 1);
        }

        Debug.Log(collision.gameObject.transform.name);
        Dead();
    }


    public override void Dead()
    {
        Debug.Log("Dead " + transform.name);
        Destroy(gameObject,.001f);
    }
}
